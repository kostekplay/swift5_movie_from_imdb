////  ViewController.swift
//  Swift5MovieFromIMDB
//
//  Created on 10/12/2020.
//  
//

import UIKit

class ViewController: UIViewController, SentMoviesDelegate, UITableViewDelegate, UITableViewDataSource  {
   
    let webservice = Webservice()
    
    var movie = [Movie]()
    
    let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
        
    }()
    
    func sentMovies(movie: [Movie]) {
        self.movie = movie
       
        movie.forEach({mov in
            print(mov.Title)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webservice.fetchJSON{(result, err) in
            if let err = err {
                print("Failed to fetch movies. ", err )
                return
            }
        }

        webservice.sentMovieDelegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("numberOfSections")
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection")
        // print(movie.count)
        return movie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("cellForRowAt")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = movie[indexPath.row].Title
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = DetailViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

