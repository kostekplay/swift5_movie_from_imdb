////  DetailViewController.swift
//  Swift5MovieFromIMDB
//
//  Created on 16/12/2020.
//  
//

import UIKit

class DetailViewController: UIViewController {

//    let imageView: UIImageView = {
//        let iv = UIImageView()
//        iv.contentMode = .scaleAspectFit
//        iv.clipsToBounds = true
//        return iv
//    }()
    
    
    let button: UIButton = {
        let btn = UIButton()
        btn.titleLabel?.text = "Button"
        btn.layer.cornerRadius = 12
        btn.backgroundColor = .green
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        button.frame = view.bounds
    }
}
