////  Models.swift
//  Swift5MovieFromIMDB
//
//  Created on 10/12/2020.
//  
//

import Foundation

struct Result: Codable {

    let Search: [Movie]

}

struct Movie: Codable {
    
    let Title: String
    let Year: String
    let imdbID: String
    // let Type: String
    let Poster: String
    
}
