////  Webservice.swift
//  Swift5MovieFromIMDB
//
//  Created on 10/12/2020.
//  
//

import Foundation

protocol SentMoviesDelegate {
    func sentMovies(movie: [Movie])
}

class Webservice {
    
    var movies = [Movie]()
    
    var sentMovieDelegate: SentMoviesDelegate!
    
    func fetchJSON(completion: @escaping (Result?, Error?)->()){

        let result: Result = Result(Search: [])
        
        let urlString = "https://www.omdbapi.com/?apikey=665cdab1&s=batman"
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            if let err = err {
                completion(nil, err)
                return
            }
    
            do {
                let results = try JSONDecoder().decode(Result.self, from: data!)
                completion(result, nil)
                
                results.Search.forEach({movie in
                    self.movies.append(movie)
                })
                
                self.sentMovieDelegate?.sentMovies(movie: self.movies)

            } catch let jsonError {
                completion(nil, jsonError)
            }
        }.resume()
    }
}
